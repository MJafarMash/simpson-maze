package graphic;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;

import logic.BlockType;
import logic.GameInfo;
import logic.PowerupType;


@SuppressWarnings("serial")
public class MapBlock extends JButton {
	private static GetEvents actionsHandler = new GetEvents();
	private Dimension location = new Dimension();
	private BlockType type;
	private PowerupType powerupType;

	private static ImageIcon foodIcon = new ImageIcon(GetNames.class.getResource("Doughnut.png"));
	private static ImageIcon duffBeer = new ImageIcon(GetNames.class.getResource("Duff.png"));
	private static ImageIcon bible    = new ImageIcon(GetNames.class.getResource("bible.png"));
	private static ImageIcon zombie   = new ImageIcon(GetNames.class.getResource("zombie.png"));
	public MapBlock(int i, int j) {
		super();
		this.setBorderPainted(false);
		this.setFocusPainted(false);
		this.setBorder(null);
		this.setSize(GameInfo.blockSizeInPixels, GameInfo.blockSizeInPixels);
		
		location.width = i;
		location.height = j;
		
		
		type = BlockType.SIMPLE;
		this.powerupType = PowerupType.NONE;
		updateBackground();
		
		this.addKeyListener(MapBlock.actionsHandler);
	}

	public void setType(BlockType type) {
		this.type = type;
		this.powerupType = PowerupType.NONE;
		updateBackground();
	}
	
	public void setType(BlockType type, PowerupType powerupType) {
		this.type = type;
		this.powerupType = powerupType;
		updateBackground();
	}

	private void updateBackground() {
		switch (type) {
		case FOOD:
			setIcon(foodIcon);
			break;
		case POWERUP:
			switch (powerupType) {
				case GHOST:
					setIcon(bible);
					break;
				case SPEED:
					setIcon(duffBeer);
					break;
				case ZOMBIE:
					setIcon(zombie);
					break;
				default:
				break;
			}
			break;
		case WALL:
			setBackground(Color.BLUE);
			break;
		default:
		case SIMPLE:
			setIcon(null);
			setBackground(Color.BLACK);
			break;
		}
//		setPressedIcon(getIcon());
	}
}


class GetEvents implements KeyListener {

	@Override
	public void keyTyped(KeyEvent e) {
	}

	@Override
	public void keyReleased(KeyEvent e) {
	}

	@Override
	public void keyPressed(KeyEvent e) {
		int keyCode = e.getKeyCode();
		switch (keyCode) {
		case KeyEvent.VK_W:
			GameInterface.getInstance().playerAction(1, logic.Direction.UP);
			break;
		case KeyEvent.VK_A:
			GameInterface.getInstance().playerAction(1, logic.Direction.LEFT);
			break;
		case KeyEvent.VK_S:
			GameInterface.getInstance().playerAction(1, logic.Direction.DOWN);
			break;
		case KeyEvent.VK_D:
			GameInterface.getInstance().playerAction(1, logic.Direction.RIGHT);
			break;
		case KeyEvent.VK_UP:
			GameInterface.getInstance().playerAction(0, logic.Direction.UP);
			break;
		case KeyEvent.VK_LEFT:
			GameInterface.getInstance().playerAction(0, logic.Direction.LEFT);
			break;
		case KeyEvent.VK_DOWN:
			GameInterface.getInstance().playerAction(0, logic.Direction.DOWN);
			break;
		case KeyEvent.VK_RIGHT:
			GameInterface.getInstance().playerAction(0, logic.Direction.RIGHT);
			break;
		default:
		}
	}
}
