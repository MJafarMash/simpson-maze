/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package graphic;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

/**
 *
 * @author mjafar
 */
@SuppressWarnings("serial")
public class GetNames extends JFrame {

	private static GameInterface gameInterface;

	private static Color badBackground = new Color(255, 240, 240);
	private static Color badBorder = new Color(142, 0, 0);
	
	private static Color goodBackground = new Color(240, 255, 240);
	private static Color goodBorder = new Color(0, 142, 0);
	
	private static Color linkHover = new Color(41,123,228);
	private static Color linkNormal = new Color(41, 59, 228);
	
	private static Cursor handCursor = new Cursor(java.awt.Cursor.HAND_CURSOR);
	private static Cursor normalCursor = new Cursor(java.awt.Cursor.DEFAULT_CURSOR);

	
    /**
     * Creates new form GetNames
     */
    public GetNames() {
    	initComponents();
    	updateStyle(txtPlayer1Name);
    	updateStyle(txtPlayer2Name);
		Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
		setLocation((screenSize.width - getWidth()) / 2,
				(screenSize.height - getHeight()) / 2);
    }

    private void initComponents() {

        lpLayers = new javax.swing.JLayeredPane();
        pnlForm = new javax.swing.JPanel();
        txtPlayer1Name = new javax.swing.JTextField();
        lblPlayer1Name = new javax.swing.JLabel();
        lblPlayer2Name = new javax.swing.JLabel();
        btnStartGame = new javax.swing.JButton();
        txtPlayer2Name = new javax.swing.JTextField();
        pnlBackground = new javax.swing.JPanel();
        lblBackGroundImage = new javax.swing.JLabel();
        pnlLinks = new javax.swing.JPanel();
        lnkAbout = new javax.swing.JLabel();
        lnkHelp = new javax.swing.JLabel();
        lnkWebsite = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("HomerMan");
        setBackground(new java.awt.Color(246, 239, 91));
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        setFocusCycleRoot(false);
        setMaximumSize(new java.awt.Dimension(620, 288));
        setMinimumSize(new java.awt.Dimension(620, 288));
        setName("StartPage"); // NOI18N
        setPreferredSize(new java.awt.Dimension(620, 288));
        setResizable(false);

        lpLayers.setMaximumSize(new java.awt.Dimension(620, 288));
        lpLayers.setMinimumSize(new java.awt.Dimension(620, 288));
        lpLayers.setPreferredSize(new java.awt.Dimension(620, 288));

        pnlForm.setBackground(new java.awt.Color(255, 211, 2));
        pnlForm.setBorder(null);

        txtPlayer1Name.setText("Homer");
        txtPlayer1Name.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        txtPlayer1Name.setFocusCycleRoot(true);
        txtPlayer1Name.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtPlayer1NameKeyPressed(evt);
            }
        });

        lblPlayer1Name.setText("نام بازیکن ۱:");

        lblPlayer2Name.setText("نام بازیکن ۲:");

        btnStartGame.setBackground(new java.awt.Color(253, 242, 51));
        btnStartGame.setText("آغاز بازی");
        btnStartGame.setBorder(null);
        btnStartGame.setFocusPainted(false);
        btnStartGame.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnStartGameMouseClicked(evt);
            }
        });

        txtPlayer2Name.setText("Bart");
        txtPlayer2Name.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        txtPlayer2Name.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtPlayer2NameKeyPressed(evt);
            }
        });
        
        rbGameMode = new JRadioButton[3];
        
        rbGameMode[0] = new JRadioButton("دو نفره");
        rbGameMode[1] = new JRadioButton("رایانه - آسان");
        rbGameMode[2] = new JRadioButton("رایانه - سخت");
        
        gameModeGroup = new ButtonGroup();
        
        gameModeGroup.add(rbGameMode[0]);
        gameModeGroup.add(rbGameMode[1]);
        gameModeGroup.add(rbGameMode[2]);
        
        rbGameMode[0].setSelected(true);
        
        rbGameMode[0].addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPlayer2Name.setEditable(true);
            }
        });
        
        rbGameMode[1].addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPlayer2Name.setEditable(false);
            }
        });
        
        rbGameMode[2].addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPlayer2Name.setEditable(false);
            }
        });
        
        javax.swing.GroupLayout pnlFormLayout = new javax.swing.GroupLayout(pnlForm);
        pnlForm.setLayout(pnlFormLayout);
        pnlFormLayout.setHorizontalGroup(
                pnlFormLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlFormLayout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(btnStartGame, javax.swing.GroupLayout.PREFERRED_SIZE, 114, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(18, 18, 18)
                    .addGroup(pnlFormLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(rbGameMode[0])
                        .addComponent(rbGameMode[1])
                        .addComponent(rbGameMode[2]))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 123, Short.MAX_VALUE)
                    .addGroup(pnlFormLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlFormLayout.createSequentialGroup()
                            .addComponent(txtPlayer2Name)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(lblPlayer2Name))
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlFormLayout.createSequentialGroup()
                            .addComponent(txtPlayer1Name, javax.swing.GroupLayout.PREFERRED_SIZE, 280, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(lblPlayer1Name)))
                    .addContainerGap())
            );
            pnlFormLayout.setVerticalGroup(
                pnlFormLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(pnlFormLayout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(pnlFormLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(pnlFormLayout.createSequentialGroup()
                            .addGroup(pnlFormLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(txtPlayer1Name, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(lblPlayer1Name))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(pnlFormLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(lblPlayer2Name)
                                .addComponent(txtPlayer2Name, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGroup(pnlFormLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnStartGame, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(rbGameMode[1]))))
                .addGroup(pnlFormLayout.createSequentialGroup()
                    .addComponent(rbGameMode[0])
                    .addGap(27, 27, 27)
                    .addComponent(rbGameMode[2]))
            );
        pnlForm.setBounds(0, 180, 620, 110);
        lpLayers.add(pnlForm, javax.swing.JLayeredPane.PALETTE_LAYER);

        pnlBackground.setForeground(new java.awt.Color(59, 154, 185));
        pnlBackground.setMaximumSize(null);
        pnlBackground.setName(""); // NOI18N
        pnlBackground.setPreferredSize(new java.awt.Dimension(620, 288));

        lblBackGroundImage.setIcon(new javax.swing.ImageIcon(this.getClass().getResource("Background.png"))); // NOI18N
        lblBackGroundImage.setPreferredSize(null);
        
        lblBackGroundImage.setText("");
        lblBackGroundImage.setSize(pnlBackground.getSize());

        javax.swing.GroupLayout pnlBackgroundLayout = new javax.swing.GroupLayout(pnlBackground);
        pnlBackground.setLayout(pnlBackgroundLayout);
        pnlBackgroundLayout.setHorizontalGroup(
            pnlBackgroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlBackgroundLayout.createSequentialGroup()
                .addComponent(lblBackGroundImage, javax.swing.GroupLayout.PREFERRED_SIZE, 620, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        pnlBackgroundLayout.setVerticalGroup(
            pnlBackgroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lblBackGroundImage, javax.swing.GroupLayout.PREFERRED_SIZE, 278, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pnlBackground.setBounds(0, 0, 620, 288);
        lpLayers.add(pnlBackground, javax.swing.JLayeredPane.DEFAULT_LAYER);

        pnlLinks.setBackground(new java.awt.Color(255, 211, 2));
        pnlLinks.setBorder(null);
        pnlLinks.setForeground(new java.awt.Color(46, 84, 227));

        lnkAbout.setForeground(new java.awt.Color(41, 59, 228));
        lnkAbout.setText("درباره");
        lnkAbout.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lnkAboutMouseClicked(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lnkAboutMouseExited(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lnkAboutMouseEntered(evt);
            }
        });

        lnkHelp.setForeground(new java.awt.Color(41, 59, 228));
        lnkHelp.setText("راهنمای بازی");
        lnkHelp.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lnkHelpMouseClicked(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lnkHelpMouseExited(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lnkHelpMouseEntered(evt);
            }
        });

        lnkWebsite.setForeground(new java.awt.Color(41, 59, 228));
        lnkWebsite.setText("سایت فارسی سیمپسون‌ها");
        lnkWebsite.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lnkWebsiteMouseClicked(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lnkWebsiteMouseExited(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lnkWebsiteMouseEntered(evt);
            }
        });

        javax.swing.GroupLayout pnlLinksLayout = new javax.swing.GroupLayout(pnlLinks);
        pnlLinks.setLayout(pnlLinksLayout);
        pnlLinksLayout.setHorizontalGroup(
            pnlLinksLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlLinksLayout.createSequentialGroup()
                .addGap(0, 1, Short.MAX_VALUE)
                .addGroup(pnlLinksLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lnkWebsite, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(lnkHelp, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(lnkAbout, javax.swing.GroupLayout.Alignment.TRAILING)))
        );
        pnlLinksLayout.setVerticalGroup(
            pnlLinksLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlLinksLayout.createSequentialGroup()
                .addComponent(lnkHelp)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lnkAbout)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 7, Short.MAX_VALUE)
                .addComponent(lnkWebsite))
        );

        pnlLinks.setBounds(450, 15, 150, 70);
        
		lpLayers.add(pnlLinks, javax.swing.JLayeredPane.MODAL_LAYER);
		
		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
		getContentPane().setLayout(layout);
		layout.setHorizontalGroup(
		    layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
		    .addComponent(lpLayers, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
		);
		layout.setVerticalGroup(
		    layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
		    .addComponent(lpLayers, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
		);
		
        getAccessibleContext().setAccessibleDescription("");

        pack();
    }          

    private void txtPlayer1NameKeyPressed(java.awt.event.KeyEvent evt) {                                          
        updateStyle(txtPlayer1Name);
    }                                         

    private void txtPlayer2NameKeyPressed(java.awt.event.KeyEvent evt) {                                          
        updateStyle(txtPlayer2Name);
    }                                         

    private void btnStartGameMouseClicked(java.awt.event.MouseEvent evt) {
    	// validate names
        if (!validateName(txtPlayer1Name.getText()) || (!validateName(txtPlayer2Name.getText()) && rbGameMode[0].isSelected())) {
        	String errorMessage = new String("نامی که برای بازیکن‌ها انتخاب کرده‌اید مناسب نیست.\n"+
        "شروع نام نباد با اعداد باشد.");
        	String errorTitle = new String("نام نا مناسب");
        	JOptionPane.showMessageDialog(rootPane, errorMessage, errorTitle, JOptionPane.ERROR_MESSAGE | JOptionPane.OK_OPTION);
        	return;
        }
        // go to next form
        if (rbGameMode[0].isSelected()) {
        	gameInterface = GameInterface.getInstance(txtPlayer1Name.getText(), txtPlayer2Name.getText());
        } else if (rbGameMode[1].isSelected()) {
        	gameInterface = GameInterface.getInstance(txtPlayer1Name.getText(), null);
        	gameInterface.gameInstance.setAI(false);
        } else {
        	gameInterface = GameInterface.getInstance(txtPlayer1Name.getText(), null);
        	gameInterface.gameInstance.setAI(true);
        }
        gameInterface.startGame();
        this.dispose();
    }                                         

    private void lnkWebsiteMouseEntered(java.awt.event.MouseEvent evt) {                                        
    	showUnderLine(lnkWebsite);
    }                                       

    private void lnkWebsiteMouseExited(java.awt.event.MouseEvent evt) {                                       
        hideUnderLine(lnkWebsite);
    }                                      

    private void lnkAboutMouseEntered(java.awt.event.MouseEvent evt) {                                      
        showUnderLine(lnkAbout);
    }                                     

    private void lnkAboutMouseExited(java.awt.event.MouseEvent evt) {                                     
    	hideUnderLine(lnkAbout);
    }                                    

    private void lnkHelpMouseEntered(java.awt.event.MouseEvent evt) {                                     
    	showUnderLine(lnkHelp);
    }                                    

    private void lnkHelpMouseExited(java.awt.event.MouseEvent evt) {                                    
    	hideUnderLine(lnkHelp);
    }              
    
    private static boolean validateName(String name) {
    	if (name.length() == 0) {
    		return false;
    	}
    	return name.matches("^[^0-9][0-9a-zA-Z^ ]{1,}$");
    }

	public static void updateStyle(JTextField textfield) {
		String text = textfield.getText();
		if (validateName(text) || text.length() == 0) {
			// Good
			textfield.setBackground(goodBackground);
			textfield.setForeground(goodBorder);
			textfield.setBorder(BorderFactory.createLineBorder(goodBorder));
		} else {
			// Bad
			textfield.setBackground(badBackground);
			textfield.setForeground(badBorder);
			textfield.setBorder(BorderFactory.createLineBorder(badBorder));
		}
	}
	
	public static void showUnderLine(JLabel l){
		l.setCursor(handCursor);
		l.setForeground(linkHover);
	}

	public static void hideUnderLine(JLabel l){
		l.setCursor(normalCursor);
		l.setForeground(linkNormal);
	}

    private void lnkHelpMouseClicked(java.awt.event.MouseEvent evt) {                                     
        String errorMessage = new String("هر بازیکنی که تعداد بیشتری دونات بخورد برنده می‌شود.\n" +
            "در حین بازی قدرت‌های ویژه ممکن است ظاهر شوند که توانایی عبور از دیوار‌ها و\n" +
        		"کشتن بازیکن حریف و افزایش سرعت حرکت را به مدت ۷ ثانیه می‌دهد\n" +
            "اگر در حالت سخت با رایانه بازی کنید و حریف توانایی کشتن\n" +
        		"شما را پیدا کند٬ بجای تمرکز بر دونات ها روی کشتن شما تمرکز می‌کند! فرار کنید!");
        String errorTitle = new String("راهنما");
        JOptionPane.showMessageDialog(rootPane, errorMessage, errorTitle, JOptionPane.INFORMATION_MESSAGE | JOptionPane.OK_OPTION);
    }                                    

    private void lnkAboutMouseClicked(java.awt.event.MouseEvent evt) {                                      
        String errorMessage = new String("برنامه‌نویسی شده با زبان برنامه‌نویسی جاوا\n" + 
                "محیط طراحی گرافیکی: NetBeans\n" +
                "محیط اصلی برنامه‌نویسی Eclipse\n" +
		"طراح و برنامه‌نویس: محمدجعفر مشهدی‌ابراهیم (شماره‌دانشجویی ۹۱۱۰۶۲۷۹)\n");
        String errorTitle = new String("درباره");
        JOptionPane.showMessageDialog(rootPane, errorMessage, errorTitle, JOptionPane.INFORMATION_MESSAGE | JOptionPane.OK_OPTION);
    }                                     

    private void lnkWebsiteMouseClicked(java.awt.event.MouseEvent evt) {                                        
        try {
         String url = "http://www.simpsons.ir";
         java.awt.Desktop.getDesktop().browse(java.net.URI.create(url));
       }
       catch (java.io.IOException e) {
           System.out.println(e.getMessage());
       }
    }                                       

    public static void main(String args[]) {
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(GetNames.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(GetNames.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(GetNames.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(GetNames.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new GetNames().setVisible(true);
            }
        });
    }
    private javax.swing.JButton btnStartGame;
    private javax.swing.JLabel lblBackGroundImage;
    private javax.swing.JLabel lblPlayer1Name;
    private javax.swing.JLabel lblPlayer2Name;
    private javax.swing.JLabel lnkAbout;
    private javax.swing.JLabel lnkHelp;
    private javax.swing.JLabel lnkWebsite;
    private javax.swing.JLayeredPane lpLayers;
    private javax.swing.JPanel pnlBackground;
    private javax.swing.JPanel pnlForm;
    private javax.swing.JPanel pnlLinks;
    private javax.swing.JTextField txtPlayer1Name;
    private javax.swing.JTextField txtPlayer2Name;
    
    private javax.swing.JRadioButton[] rbGameMode;
    private javax.swing.ButtonGroup gameModeGroup;
}
