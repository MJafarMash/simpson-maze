package graphic;
import java.awt.Color;
import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;

import logic.Simpmaze;

@SuppressWarnings("serial")
public class GameInfoDisplay extends JPanel {
	private static GameInfoDisplay instance;
	private Simpmaze gameInstance;
	private JLabel[] playersInfo = new JLabel[2];
	
	public static GameInfoDisplay getInstance(Simpmaze gameInstance) {
		if (instance == null) {
			instance = new GameInfoDisplay(gameInstance);
		}
		
		return instance;
	}
	
	private GameInfoDisplay(Simpmaze gameInstance) {
		this.gameInstance = gameInstance;
		this.setLayout(new GridLayout(1, 2));
		
		playersInfo[0] = new JLabel();
		playersInfo[1] = new JLabel();
		
		playersInfo[0].setSize(getWidth(), 20);
		playersInfo[1].setSize(getWidth(), 20);
		
		playersInfo[0].setForeground(Color.WHITE);
		playersInfo[1].setForeground(Color.WHITE);
		
		update();
		
		this.add(playersInfo[0]);
		this.add(playersInfo[1]);
		
		this.setSize(getWidth(), 40);
	}
	
	private GameInfoDisplay() {}
	
	private String playerInfoPlayerText(int playerNumber) {
		String playerName = gameInstance.getInfo().getPlayerName(playerNumber);
		return String.format("%s: %2d", 
				(playerName.length() < 10 ? playerName : playerName.substring(0, 10) + "..."), 
				gameInstance.getInfo().getPlayerScore(playerNumber));
	}

	public void update() {
		playersInfo[0].setText(playerInfoPlayerText(0));
		playersInfo[1].setText(playerInfoPlayerText(1));	
	}
}