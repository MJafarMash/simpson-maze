package graphic;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class GameStep implements ActionListener {
	GameInterface gameInterface;
	javax.swing.Timer timer;
	public GameStep(GameInterface gameInterface) {
		this.gameInterface = gameInterface;
		timer = new javax.swing.Timer(250, this);
		timer.start();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
		boolean thereIsWinner;
		thereIsWinner = gameInterface.gameInstance.step();
		if (!thereIsWinner) {
			gameInterface.map.repaint();
			gameInterface.info.repaint();
			gameInterface.update();
		} else {
			gameInterface.gameFrame.dispose();
			timer.stop();
			new WinnerWindow(gameInterface.gameInstance.getWinner());
		}
		
	}
}