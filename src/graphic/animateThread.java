package graphic;

import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JLabel;

public class animateThread extends Thread {
	public JLabel object;
	public Point newLocation;
	public double dx, dy;
	public javax.swing.Timer timer;
	public animateThread(Player player, Point pixelLocation) {
		object = player;
		newLocation = pixelLocation;
		dx = (newLocation.x - object.getLocation().x) / 25;
		dy = (newLocation.y - object.getLocation().y) / 25; 
	}
	
	
	@Override
	public synchronized void run() {
		super.run();
		if (dx == 0 && dy == 0) {
			return;
		}
		Animate runner = new Animate(this);
		timer = new javax.swing.Timer(10, runner);
		timer.start();
		System.out.println("timer stopped");
	}
	
}


class Animate implements ActionListener {
	public int frame = 0;
	private animateThread thread;
	
	public Animate(animateThread t) {
		thread = t;
	}
	
	@Override
	public synchronized void actionPerformed(ActionEvent e) {
		if (frame >= 25) {
			thread.timer.stop();
		}
//		double speed = Math.cos(frame*Math.PI/50.0);
		thread.object.setLocation((int)(thread.object.getLocation().x + thread.dx),
								  (int)(thread.object.getLocation().y + thread.dy));
		frame++;
	}
}