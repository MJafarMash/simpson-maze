package graphic.sound;

import javazoom.jl.player.jlp;

public class SoundManager {
	
	public static void homerDoh() {
		int DohNumber = logic.Simpmaze.rndMaker.nextInt(18) + 1;
		play(String.format("DOH-%d.mp3",DohNumber));
	}
	
	public static void bartDoh() {
		play("aycaramba.mp3");
	}
	
	public static void bartVictory() {
		play("bartvictory.mp3");
	}
	
	public static void homerVictory() {
		play("woo-hoo.mp3");
	}
	
	public static void zombie() {
		play("whyyoulittle.mp3");
	}
	
	public static void ghost() {
		// TODO
	}
	
	public static void speed() {
		play("duffman.mp3");
	}
	
	public static void theme() {
		// TODO: get sound
		play("themesong.mp3");
	}

	public static void score() {
		int eatNumber = logic.Simpmaze.rndMaker.nextInt(3) + 1;
		play(String.format("EAT-%d.mp3",eatNumber));
	}
	
	public static void play(String fileName) {
		String fileAddr = "";
		fileAddr = SoundManager.class.getResource(fileName).getPath();
		new playThread(fileAddr).start();
	}
}

class playThread extends Thread {
	private String fileAddress;
	playThread(String fileAddress) {
		this.fileAddress = fileAddress;
	}
	
	@Override 
	public void run() {
		super.run();
		try {
			jlp.createInstance(new String[]{fileAddress}).play();
		} catch (Exception e) {
			System.err.println("play:" + fileAddress);
			e.printStackTrace();
		}
	}
}
