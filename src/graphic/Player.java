package graphic;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

import logic.GameInfo;

@SuppressWarnings("serial")
public class Player extends JLabel {
	public static ImageIcon homer;
	public static ImageIcon bart;
	
	public static ImageIcon homerMad;
	public static ImageIcon bartMad;
	
	private int playerID;
	
	static {
		homer    = new ImageIcon(GetNames.class.getResource("homer.png"));
		homerMad = new ImageIcon(GetNames.class.getResource("HomerMad.png"));
		bart     = new ImageIcon(GetNames.class.getResource("bart.png"));
		bartMad  = new ImageIcon(GetNames.class.getResource("bartmad.png"));
	}

	public Player(int playerID) {
		this.playerID = playerID;
		setSize(GameInfo.blockSizeInPixels, GameInfo.blockSizeInPixels);
		setVisible(true);
		setNormal();
	}
	
	public void setMad() {
		if (playerID == 0) {
			// Homer
			setIcon(homerMad);
		} else if (playerID == 1) {
			// Bart
			setIcon(bartMad);
		}
	}
	
	public void setNormal() {
		if (playerID == 0) {
			// Homer
			setIcon(homer);
		} else if (playerID == 1) {
			// Bart
			setIcon(bart);
		}	
	}
	
}
