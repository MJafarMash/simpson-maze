package graphic;
import java.awt.GridLayout;
import java.awt.Point;

import javax.swing.JLayeredPane;
import javax.swing.JPanel;

import logic.BlockType;
import logic.GameInfo;
import logic.PowerupType;
import logic.Simpmaze;


@SuppressWarnings("serial")
public class GameMap extends JPanel {
	private static GameMap instance;
	
	private Simpmaze gameInstance;
	
	private JLayeredPane layers = new JLayeredPane();
	private MapBtnArray mapArray;
	public Player[] players; 
	
	private static final Integer GROUNDLAYER = new Integer(1);
	private static final Integer PLAYERSLAYER = new Integer(2);
	
	public static GameMap getInstance(Simpmaze gameInstance) {
		if (instance == null) {
			instance = new GameMap(gameInstance);
		}
		
		return instance;
	}
	
	private GameMap(Simpmaze gameInstance) {
		this.gameInstance = gameInstance;
		
		players = new Player[2];
		players[0] = new Player(0);
		players[1] = new Player(1);
		
		mapArray = new MapBtnArray(gameInstance);
		
		this.setSize(GameInfo.blockSizeInPixels * GameInfo.getMapWidth(),
					 GameInfo.blockSizeInPixels * GameInfo.getMapHeight());
		this.setLayout(null);
		
		mapArray.setSize(this.getSize());
		
		players[0].setLocation(getPixelLocation(0));
		players[1].setLocation(getPixelLocation(1));
		update();
		
		layers.setBounds(0, 0, getWidth(), getHeight());
		layers.add(mapArray, GROUNDLAYER);
		layers.add(players[0], PLAYERSLAYER);
		layers.add(players[1], PLAYERSLAYER);
		
		this.add(layers);
	}
	
	private Point getPixelLocation(int playerID) {
		Point playerLocation = gameInstance.getInfo().getPlayerLocation(playerID); 
		return new Point(playerLocation.x * GameInfo.blockSizeInPixels,
						 playerLocation.y * GameInfo.blockSizeInPixels);
	}
	
	private GameMap() {}

	public void update() {
		for (int i = 0; i < GameInfo.getMapHeight(); i++) {
			for (int j = 0; j < GameInfo.getMapWidth(); j++) {
				if (gameInstance.getType(i, j) != BlockType.POWERUP) {
					mapArray.btnArray[i][j].setType(gameInstance.getType(i, j));
				} else {
					mapArray.btnArray[i][j].setType(gameInstance.getType(i, j), gameInstance.getPowerupType(i, j));
				}
			}
		}
		//
//		new animateThread(players[0], getPixelLocation(0)).start();
//		new animateThread(players[1], getPixelLocation(1)).start();
		players[0].setLocation(getPixelLocation(0));
		players[1].setLocation(getPixelLocation(1));
		//
		if (gameInstance.getInfo().playerHasPower(0, PowerupType.ZOMBIE)) {
			players[0].setMad();
		} else {
			players[0].setNormal();
		}
		if (gameInstance.getInfo().playerHasPower(1, PowerupType.ZOMBIE)) {
			players[1].setMad();
		} else {
			players[1].setNormal();
		}
		//
		repaint();
	}
}

@SuppressWarnings("serial")
class MapBtnArray extends JPanel {
	private Simpmaze gameInstance;
	public MapBlock[][] btnArray;
	
	public MapBtnArray(Simpmaze gameInstance) {
		this.gameInstance = gameInstance;
		GridLayout layout = new GridLayout(GameInfo.getMapHeight(), GameInfo.getMapWidth());
		layout.setHgap(0);
		layout.setVgap(0);
		this.setLayout(layout);
		
		btnArray = new MapBlock[GameInfo.getMapHeight()][GameInfo.getMapWidth()];
		for (int i = 0; i < GameInfo.getMapHeight(); i++) {
			for (int j = 0; j < GameInfo.getMapWidth(); j++) {
				btnArray[i][j] = new MapBlock(i, j);
				this.add(btnArray[i][j]);
			}
		}	
	}
	
	public void updateIcons() {
		for (int i = 0; i < GameInfo.getMapHeight(); i++) {
			for (int j = 0; j < GameInfo.getMapWidth(); j++) {
				btnArray[i][j].setType(gameInstance.getType(i, j));
			}
		}
	}
}