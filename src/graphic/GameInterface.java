package graphic;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JFrame;

import logic.Direction;
import logic.GameInfo;
import logic.Simpmaze;

public class GameInterface {
	public Simpmaze gameInstance;

	public JFrame gameFrame;
	public GameMap map;
	public GameInfoDisplay info;

	private static GameInterface selfInstance;
	
	public static GameInterface getInstance() {
		if (selfInstance == null) {
			throw new NullPointerException("Game interface not initialized");
		}
		
		return selfInstance;
	}
	
	public static GameInterface getInstance(String player1Name, String player2Name) {
		if (selfInstance == null) {
			selfInstance = new GameInterface(player1Name, player2Name);
		}
		
		return getInstance();
	}
	
	private GameInterface() {}
	
	private GameInterface(String player1Name, String player2Name) {		
		gameFrame = new JFrame("SimpMaze");
		gameInstance = new Simpmaze();
		gameInstance.setNames(player1Name, player2Name);
		initFrame();
	}

	private void initFrame() {
		try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Metal".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(GetNames.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(GetNames.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(GetNames.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(GetNames.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
		
		gameFrame.setLayout(null);
		gameFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		map = GameMap.getInstance(gameInstance);
		info = GameInfoDisplay.getInstance(gameInstance);
		info.setBackground(Color.BLUE);


		map.setLocation(0, 0);
		map.setSize(GameInfo.blockSizeInPixels * GameInfo.getMapWidth(),
				GameInfo.blockSizeInPixels * GameInfo.getMapHeight());

		info.setLocation(0, map.getHeight());
		info.setSize(map.getWidth(), 110);

		gameFrame.add(map);
		gameFrame.add(info);
		
		gameFrame.setSize(map.getWidth(), map.getHeight() + info.getHeight());
		gameFrame.setResizable(false);
		Dimension screenSize = java.awt.Toolkit.getDefaultToolkit()
				.getScreenSize();
		gameFrame.setLocation((screenSize.width - gameFrame.getWidth()) / 2,
				(screenSize.height - gameFrame.getHeight()) / 2);
	}
	
	public void startGame() {
		gameFrame.setVisible(true);
		new GameStep(this);
	}

	public void playerAction(int playerNumber, Direction direction) {
		gameInstance.playerAction(playerNumber, direction);
	}
	
	public void update() {
		map.update();
		info.update();
	}
}