package graphic;

import graphic.sound.SoundManager;

import java.awt.Dimension;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;

@SuppressWarnings("serial")
public class WinnerWindow extends JFrame {
	
	protected JLabel background= new JLabel();
	protected JLabel score = new JLabel();
	protected JLayeredPane layers = new JLayeredPane();
	protected Integer PICTURELAYER = new Integer(1);
	protected Integer SCORELAYER = new Integer(2);
	
	
	public WinnerWindow(int playerID) {
		super();
		this.setResizable(false);
//		this.setLayout(null);
		this.setTitle("We have a winner... ghrrrrr... doughnuts!");
		// TODO: bad az tahvile tamrin, do you want to play again bezaram :)
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		//
		this.setBounds(0, 0, 581, 409);
		background.setBounds(0, 0, this.getWidth(), this.getHeight());
		layers.setBounds(background.getBounds());
		// 
		Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
		setLocation((screenSize.width - getWidth()) / 2,
					(screenSize.height - getHeight()) / 2);
		//
		score.setFont(new java.awt.Font("Cantarell", 1, 18)); 
        score.setForeground(new java.awt.Color(238, 238, 238));
        
        score.setLocation(263, 0);
        score.setText(String.format(
        		"%s برنده شد! امتیاز: %d",
        		GameInterface.getInstance().gameInstance.getInfo().getPlayerName(playerID),
        		GameInterface.getInstance().gameInstance.getInfo().getPlayerScore(playerID)));

        score.setSize(400, 300);
		//
		if(playerID == 0) {
			SoundManager.homerVictory();
			background.setIcon(new ImageIcon(GameInterface.class.getResource("homerwin.jpg")));
		} else {
			SoundManager.bartVictory();
			background.setIcon(new ImageIcon(GameInterface.class.getResource("bartwin.jpg")));
		}
		
		layers.add(background, PICTURELAYER);
		layers.add(score, SCORELAYER);
		add(layers);
		this.setVisible(true);
	}
}
