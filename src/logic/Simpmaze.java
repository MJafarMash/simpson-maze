package logic;

import graphic.sound.SoundManager;

import java.awt.Point;
import java.util.Random;

import logic.astar.AStar;
import logic.astar.Node;

public class Simpmaze {
	private GameInfo info = new GameInfo();
	public static Random rndMaker = new Random();
	private int winnerID = -1;
	
	private boolean bartIsComputer = false;
	private boolean realAI = true;
	
	private int powerupShowFrequency = 25;
	
	private int time = 0;
	private int lastTimePutPrizeInMap = 0;
	
	public BlockType getType(int i, int j) {
		return info.getMap()[i][j].getType();
	}

	public void setNames(String player1Name, String player2Name) {
		if (player2Name == null) {
			info.playerSetNames(player1Name, "Fritz");
			bartIsComputer = true;
		} else {
			info.playerSetNames(player1Name, player2Name);
			bartIsComputer = false;
		}
	}
	
	public void setAI(boolean realAI) {
		this.realAI = realAI;
	}

	public GameInfo getInfo() {
		return info;
	}

	public void playerAction(int playerNumber, Direction direction) {
		if (playerNumber == 1 && bartIsComputer == true) {
			return;
		}
		info.playerChangeDirection(playerNumber, direction);
	}

	public boolean step() {
		boolean homerAteBart = false,
				bartAteHomer = false;
		time++;
		// Do AI Things
		if (bartIsComputer) {
			doAI();
		}
		// MOVE
		if (time % 2 == 1) {
			homerAteBart = stepMovePlayer(0);
			bartAteHomer = stepMovePlayer(1);
		} else {
			if (info.playerHasPower(0, PowerupType.SPEED)) {
				homerAteBart = stepMovePlayer(0);
			}
			if (info.playerHasPower(1, PowerupType.SPEED)) {
				bartAteHomer = stepMovePlayer(1);
			}
		}
		// EAT
		eatDoughnut(0);
		eatDoughnut(1);
		// LOSE POWERS
		info.playerPasstime(0);
		info.playerPasstime(1);
		// PUT POWERS IN MAP
		if (bartIsComputer && realAI) {
			powerupShowFrequency = 15;
		}
		if ((time/4) % powerupShowFrequency == rndMaker.nextInt(4) && (time/powerupShowFrequency) != lastTimePutPrizeInMap) {
			lastTimePutPrizeInMap = time / powerupShowFrequency;
//			if (rndMaker.nextInt(4) == 1) {
//				info.foodAddToMap(info.getFreeSpace());
//			} else {
				info.powerupAddToMap(info.getFreeSpace());
//			}
		}
		// END OF GAME CONDITIONS
		if (!info.foodRemainsInMap() || homerAteBart || bartAteHomer) {
			if (homerAteBart) {
				winnerID = 0;
			} else if (bartAteHomer) {
				winnerID = 1;
			} else if (info.getPlayerScore(0) > info.getPlayerScore(1)) {
				winnerID = 0;
			} else {
				winnerID = 1;
			}
			return true;
		}
		return false;
	}
	
	private void doAI() {
		// Find next node
		Point currentLocation = info.getPlayerLocation(1);
		int nextNodeCol, nextNodeRow;
		if (realAI) {
			AStar[] pathFinders;
			Point goalLocation;
			int minimumIndex;
			if (info.getNumberrOfFood() == 0) {
				return;
			}
			if (info.playerHasPower(1, PowerupType.ZOMBIE)) {
				// Find homer instead of food
				pathFinders = new AStar[1];
				minimumIndex = 0;
				goalLocation = info.getPlayerLocation(0);
				pathFinders[0] = new AStar(currentLocation.y, currentLocation.x, goalLocation.y, goalLocation.x);
			} else {
				// Find Nearest Food
				pathFinders = new AStar[info.getNumberrOfFood()];
				minimumIndex = 0;
				for (int i = 0; i < info.getNumberrOfFood() ; i++) {
					goalLocation = info.getFoodLocations().get(i);
					pathFinders[i] = new AStar(currentLocation.y, currentLocation.x, goalLocation.y, goalLocation.x);
					if (pathFinders[i].getPathLength() < pathFinders[minimumIndex].getPathLength()) {
						minimumIndex = i;
					}
				}
			}
			// Go there
			Node nextNode = pathFinders[minimumIndex].getNextMove();
			nextNodeRow = nextNode.getRow();
			nextNodeCol = nextNode.getCol();
		} else {
			int newDirection;
			do {
				newDirection = rndMaker.nextInt(4);
			} while (!info.canChangeDirection(1, newDirection));
			
			nextNodeCol = nextNodeRow = 0;
			switch (newDirection) {
			case 0:
				nextNodeRow = currentLocation.y - 1;
				break;				
			case 1:
				nextNodeCol = currentLocation.x + 1;
				break;
			case 2:
				nextNodeRow = currentLocation.y + 1;
				break;
			case 3:
				nextNodeCol = currentLocation.x - 1;
				break;
			default:
				break;
			}
		}
		// change direction if necessary
		if (nextNodeRow == currentLocation.y - 1) {
			info.playerChangeDirection(1, Direction.UP);
		} else if(nextNodeRow == currentLocation.y + 1) {
			info.playerChangeDirection(1, Direction.DOWN);
		} else if(nextNodeCol == currentLocation.x - 1) {
			info.playerChangeDirection(1, Direction.LEFT);
		} else if(nextNodeCol == currentLocation.x + 1) {
			info.playerChangeDirection(1, Direction.RIGHT);
		}
	}

	private void eatDoughnut(int playerID) {
		Point playerLocation = info.getPlayerLocation(playerID);
		MapBlock blockStandsOn = info.getMap()[playerLocation.y][playerLocation.x];
		switch (blockStandsOn.getType()) {
		case FOOD:
			info.score(playerID, 1);
			info.foodRemoveFromMap(playerLocation);
			SoundManager.score();
		case POWERUP:
			PowerupType powerupType = blockStandsOn.getPowerupType();
			blockStandsOn.eatPowerup();
			switch(powerupType) {
			case ZOMBIE:
				info.playerGainPower(playerID, powerupType);
				SoundManager.zombie();
				break;
			case SPEED:
				info.playerGainPower(playerID, powerupType);
				SoundManager.speed();
				break;
			case GHOST:
				info.playerGainPower(playerID, powerupType);
				SoundManager.ghost();
				break;
			case NONE:
			default:
			}
		case SIMPLE:
		case WALL:
		default:
		}
	}

	private boolean stepMovePlayer(int playerID) {
		int otherPlayerID = (playerID + 1) % 2;
		if (info.getPlayerNextPos(playerID).equals(info.getPlayerLocation(otherPlayerID))){
			if (info.playerHasPower(playerID, PowerupType.ZOMBIE)) {
				return true;
			} else {
				info.playerReverseDirection(playerID);
				info.playerReverseDirection(otherPlayerID);
				SoundManager.homerDoh();
				SoundManager.bartDoh();
			}
		}
		if(info.gameMap[info.getPlayerNextPos(playerID).y][info.getPlayerNextPos(playerID).x].getType() == BlockType.WALL && 
						!info.playerHasPower(playerID, PowerupType.GHOST)) {
			info.playerReverseDirection(playerID);
			if (playerID == 0) {
				SoundManager.homerDoh();
			} else {
				SoundManager.bartDoh();
			}
		}
		
		info.playerMove(playerID);
		
		return false;
	}

	public PowerupType getPowerupType(int i, int j) {
		return info.getMap()[i][j].getPowerupType();
	}

	public int getWinner() {
		return winnerID;
	}
}
