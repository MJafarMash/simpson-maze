package logic;

import java.awt.Dimension;
import java.awt.Point;
import java.util.LinkedList;
import java.util.Scanner;

import logic.astar.AStar;

public class GameInfo {
	private static Dimension mapSize = new Dimension();

	public static int blockSizeInPixels = 25;
	public static int numberOfFood = 8;
	
	public MapBlock[][] gameMap;
	private Player[] players = new Player[2];
	
	private LinkedList<Point> foodLocations = new LinkedList<Point>();

	public GameInfo() {
		loadMap();
		players[0] = new Player();
		players[1] = new Player();
		
		players[0].setLocation(getFreeSpace());
		players[1].setLocation(getFreeSpace());
		
		initializeDirection(0);
		initializeDirection(1);

		Point foodPlace;
		for (int i = 0; i < numberOfFood; i++) {
			foodPlace = getFreeSpace();
			foodAddToMap(foodPlace);
		}
	}
	
	private void initializeDirection(int playerID) {
		int newDirection;
		do {
			newDirection = Simpmaze.rndMaker.nextInt(4);
		} while (!canChangeDirection(playerID, newDirection));
		
		switch (newDirection) {
		case 0:
			players[playerID].changeDirection(Direction.UP);
			break;
		case 1:
			players[playerID].changeDirection(Direction.RIGHT);
			break;
		case 2:
			players[playerID].changeDirection(Direction.DOWN);
			break;
		case 3:
			players[playerID].changeDirection(Direction.LEFT);
			break;
		}
	}

	public Point getFreeSpace() {
		int row, col;
		Point selectedLocation;
		boolean isGood;
		do {
			row = Simpmaze.rndMaker.nextInt(mapSize.height);
			col = Simpmaze.rndMaker.nextInt(mapSize.width);
			
			selectedLocation = new Point(col, row);
			
			isGood = gameMap[row][col].getType() == BlockType.SIMPLE
					&& !players[0].getLocation().equals(selectedLocation)
					&& !players[1].getLocation().equals(selectedLocation);
			
			for (int i = -2; i <= 2; i++) {
				for (int j = -2; j <= 2; j++) {
					try {
						if (gameMap[row + i][col + j].getType() != BlockType.SIMPLE &&
							gameMap[row + i][col + j].getType() != BlockType.WALL) {
							isGood = false;
						}
					}catch(ArrayIndexOutOfBoundsException e) {
						// do nothing :D
					}
				}
			}
		} while (!isGood);
		
		return selectedLocation;
	}
	
	public LinkedList<Point> getFoodLocations() {
		return foodLocations;
	}
	
	public int getNumberrOfFood() {
		return foodLocations.size();
	}

	public MapBlock[][] getMap() {
		return gameMap;
	}

	public static int getMapWidth() {
		return mapSize.width;
	}

	public static int getMapHeight() {
		return mapSize.height;
	}

	public int getPlayerScore(int playerID) {
		return players[playerID].getScore();
	}

	public String getPlayerName(int playerID) {
		return players[playerID].getName();
	}
	
	public Point getPlayerLocation(int playerID) {
		return players[playerID].getLocation();
	}
	
	public Point getPlayerNextPos(int playerID) {
		Point location, velocity;
		location = getPlayerLocation(playerID);
		velocity = getPlayerVelocity(playerID);
		return new Point(location.x + velocity.x, location.y + velocity.y);
	}
	
	public Point getPlayerVelocity(int playerID) {
		return new Point(players[playerID].getVelocity().width, players[playerID].getVelocity().height);
	}
	
	public boolean playerHasPower(int playerID, PowerupType powerupType) {
		return players[playerID].hasPower(powerupType);
	}

	public void score(int playerID, int score) {
		players[playerID].addScore(score);
	}
	
	public void playerGainPower(int playerID, PowerupType powerupType) {
		players[playerID].setPower(powerupType);
	}
	
	public void playerReverseDirection(int playerID) {
		players[playerID].reverseDirection();
	}
	
	public void playerMove(int playerID) {
		players[playerID].setLocation(getPlayerNextPos(playerID));
	}
	
	public void playerSetNames(String player1Name, String player2Name) {
		players[0].setName(player1Name);
		players[1].setName(player2Name);
	}

	public void playerChangeDirection(int playerID, Direction direction) {
		if (!playerHasPower(playerID, PowerupType.GHOST)) {
			if (!canChangeDirection(playerID, direction)) {
				if (playerID == 0) {
					graphic.sound.SoundManager.homerDoh();
				} else {
					graphic.sound.SoundManager.bartDoh();
				}
				return;
			}
		}
		players[playerID].changeDirection(direction);
	}

	public void playerPasstime(int playerID) {
		for (int i = players[playerID].powerTimes.size() - 1; i >= 0; i--) {
			players[playerID].powerTimes.set(i, players[playerID].powerTimes.get(i) + 1);
			if (players[playerID].powerTimes.get(i) >= 7 * 4) {
				players[playerID].powerTimes.remove(i);
				players[playerID].powers.remove(i);
			}
		}
	}

	private void loadMap() {
		Scanner in = new Scanner(Simpmaze.class.getResourceAsStream("Map"));
		
		mapSize.width = in.nextInt();
		mapSize.height = in.nextInt();
		in.nextLine();

		gameMap = new MapBlock[mapSize.height][mapSize.width];
		boolean[][] AStarMap = new boolean[mapSize.height][mapSize.width];
		for (int i = 0; i < mapSize.height; i++) {
			for (int j = 0; j < mapSize.width; j++) {
				switch (in.next().toCharArray()[0]) {
				case '#':
					gameMap[i][j] = new MapBlock(BlockType.WALL);
					AStarMap[i][j] = false;
					break;
				case '-':
					gameMap[i][j] = new MapBlock(BlockType.SIMPLE);
					AStarMap[i][j] = true;
					break;
				}
			}
		}
		
		AStar.setMap(AStarMap);
		
		in.close();
	}
	
	public boolean foodRemainsInMap() {
		return foodLocations.size() > 0;
	}

	public void foodAddToMap(Point location) {
		foodLocations.add(location);
		gameMap[location.y][location.x].putFood();
	}
	
	public void foodRemoveFromMap(Point location) {
		foodLocations.remove(location);
		gameMap[location.y][location.x].eatFood();
	}
	
	public void powerupAddToMap(Point location) {
		PowerupType powerUpType = PowerupType.NONE;
		switch (Simpmaze.rndMaker.nextInt(3)) {
		case 0:
			powerUpType = PowerupType.ZOMBIE;
			break;
		case 1:
			powerUpType = PowerupType.SPEED;
			break;
		case 2: 
			powerUpType = PowerupType.GHOST;
			break;
		}
		gameMap[location.y][location.x].putPowerup(powerUpType);
		System.err.printf("+P:[%d,%d]:%s\n", location.x, location.y, powerUpType.toString());
	}

	public boolean canChangeDirection(int playerID, int newDirection) {
		switch (newDirection) {
		case 0:
			return canChangeDirection(playerID, Direction.UP);
		case 1:
			return canChangeDirection(playerID, Direction.RIGHT);
		case 2:
			return canChangeDirection(playerID, Direction.DOWN);
		case 3:
			return canChangeDirection(playerID, Direction.LEFT);
		default:
			return false;
		}
	}
	
	public boolean canChangeDirection(int playerID, Direction direction) {
		Point location = getPlayerLocation(playerID);
		return 
		   (direction == Direction.RIGHT && gameMap[location.y][location.x + 1].getType() != BlockType.WALL) 
		|| (direction == Direction.LEFT && gameMap[location.y][location.x - 1].getType() != BlockType.WALL) 
		|| (direction == Direction.UP && gameMap[location.y - 1][location.x].getType() != BlockType.WALL) 
		|| (direction == Direction.DOWN && gameMap[location.y + 1][location.x].getType() != BlockType.WALL);
	}
}
