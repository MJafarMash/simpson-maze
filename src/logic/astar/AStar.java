package logic.astar;

import java.util.LinkedList;

public class AStar {
	
	private Node start, end;
	private LinkedList<Node> route = new LinkedList<Node>();
	private int routeIndex = 0;
	
	private LinkedList<Node> closedNodes = new LinkedList<Node>();
	private LinkedList<Node> openNodes = new LinkedList<Node>();
	
	private static boolean map[][];
	
	public static void setMap(boolean __map[][]) {
		map = __map;
	}
	
	public AStar(int fromRow, int fromCol, int toRow, int toCol) {
		end = new Node(toRow, toCol, null, null);
		start = new Node(fromRow, fromCol, null, end);
		
		findPath();
	}
	
	public int getPathLength() {
		return route.size();
	}
	
	public Node getNextMove() {
		return route.get(routeIndex++);
	}
	
	private void findPath() {
		// add start node to the openSet
		openNodes.add(start);

		Node currentNode = start;
		while(openNodes.size() > 0) {
			// get current node
		    currentNode = getSmallestFvaluedNode();
		    // add adjacent nodes to open list
		    addToOpenList(currentNode.getRow() - 1, currentNode.getCol(), currentNode);
		    addToOpenList(currentNode.getRow() + 1, currentNode.getCol(), currentNode);
		    addToOpenList(currentNode.getRow(), currentNode.getCol() - 1, currentNode);
		    addToOpenList(currentNode.getRow(), currentNode.getCol() + 1, currentNode);
		    // move current node from open nodes to closed nodes
		    closedNodes.add(currentNode);
		    openNodes.remove(currentNode);
		    //if (findInNodes(closedNodes, end.getRow(), end.getCol()) != -1) {
		    if (currentNode.equals(end)) {
		    	break;
		    }
		}
		// found path
		// in this map no route to destination will not happen
		while (!start.equals(currentNode) /*&& currentNode != null */ ) {
			route.addFirst(currentNode);
			currentNode = currentNode.getParent();
		}
	}

	private Node getSmallestFvaluedNode() {
		Node smallest = openNodes.get(0);
		for (Node thisNode : openNodes) {
			if(thisNode.getFScore() < smallest.getFScore()) {
				smallest = thisNode;
			}
		}
		return smallest;
	}
	
	private int findInNodes(LinkedList<Node> list, int row, int col) {
		Node thisNode;
		for(int i = 0; i < list.size(); i++) {
			thisNode = list.get(i);
			if (thisNode.getRow() == row && thisNode.getCol() == col) {
				return i;
			}
		}
		return -1;
	}
	
	private void addToOpenList(int row, int col, Node currentNode) {
		try {
			int openListID;
	    	if (map[row][col] && findInNodes(closedNodes, row, col) == -1) {
	    		openListID = findInNodes(openNodes, row, col);
	    		if (openListID == -1) {
	    			openNodes.add(new Node(row, col, currentNode, end));
	    		} else {
	    			openNodes.get(openListID).changeParent(currentNode);
	    		}
	    	}
	    } catch(ArrayIndexOutOfBoundsException e) {}
	}
	
	@Deprecated
	public int isInRoute(int row, int col) {
		Node thisNode;
		for(int i = 0; i < route.size(); i++) {
			thisNode = route.get(i);
			if (thisNode.getRow() == row && thisNode.getCol() == col) {
				return i;
			}
		}
		return -1;
	}

}