package logic.astar;


import java.util.Scanner;

public class AStarTest {

	int mapSizeCols;
	int mapSizeRows;

	boolean[][] AStarMap;
	public void setUp() {
		Scanner in = new Scanner(AStar.class.getResourceAsStream("AStarTestMap"));

		mapSizeCols = in.nextInt();
		mapSizeRows = in.nextInt();
		in.nextLine();
		
		AStarMap = new boolean[mapSizeRows][mapSizeCols];
		for (int i = 0; i < mapSizeRows; i++) {
			for (int j = 0; j < mapSizeCols; j++) {
				switch (in.next().toCharArray()[0]) {
				case '#':
					AStarMap[i][j] = false;
					break;
				case '-':
					AStarMap[i][j] = true;
					break;
				}
			}
		}

		AStar.setMap(AStarMap);

		in.close();
	}

	public void test() {
		AStar pathFinder = new AStar(5, 1, 5, 13);
		for (int i = 0; i < mapSizeRows; i++) {
			for (int j = 0; j < mapSizeCols; j++) {
				if (!AStarMap[i][j]) {
					System.out.print("--");
				} else {
					int id = pathFinder.isInRoute(i,j);
					if (id == -1) {
						System.out.print("  ");
					} else {
//						System.out.printf("%2d", id);
						System.out.print(" * ");
					}
				}
			}
			System.out.println();
		}
	}

	public static void main(String[] args) {
		AStarTest a = new AStarTest();
		a.setUp();
		a.test();
	}

}
