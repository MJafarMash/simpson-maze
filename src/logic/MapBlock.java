package logic;

public class MapBlock {
	private BlockType type;
	private PowerupType power;

	public MapBlock(BlockType t) {
		type = t;
		power = PowerupType.NONE;
	}

	public BlockType getType() {
		return type;
	}

	public void eatFood() {
		type = BlockType.SIMPLE;
	}
	
	public void eatPowerup() {
		type = BlockType.SIMPLE;
		power = PowerupType.NONE;
	}

	public void putPowerup(PowerupType powerUpType) {
		type = BlockType.POWERUP;
		power = powerUpType;
	}
	
	public PowerupType getPowerupType() {
		return power;
	}

	public void putFood() {
		type = BlockType.FOOD;
	}
}
