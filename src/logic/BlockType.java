package logic;

public enum BlockType {
	WALL,
	SIMPLE,
	FOOD,
	POWERUP
}
