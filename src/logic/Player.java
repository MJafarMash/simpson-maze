package logic;

import java.awt.Dimension;
import java.awt.Point;
import java.util.LinkedList;

public class Player {
	private String name ;
	private int score = 0;
	private Point location;
	public  LinkedList<PowerupType> powers = new LinkedList<PowerupType>();
	public  LinkedList<Integer> powerTimes = new LinkedList<Integer>();
	private Dimension velocity;
	
	public Player() {
		setLocation(new Point(0, 0));
		velocity = new Dimension(0, 0);
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getScore() {
		return score;
	}
	public void addScore(int score) {
		this.score += score;
	}
	public Point getLocation() {
		return location;
	}
	public void setLocation(Point location) {
		this.location = location;
	}
	public LinkedList<PowerupType> getPowers() {
		return powers;
	}
	public void setPower(PowerupType power) {
		this.powers.addLast(power);
		this.powerTimes.addLast(new Integer(0));
	}
	public Direction getDirection() {
		if (velocity.width == 1) {
			return Direction.RIGHT;
		} else if (velocity.width == -1) {
			return Direction.LEFT;
		} else if (velocity.height == -1) {
			return Direction.UP;
		} else if (velocity.height == 1) {
			return Direction.DOWN;
		} else {
			return Direction.NOTMOVING;
		}
	}
	public void reverseDirection() {
		velocity.height *= -1;
		velocity.width *= -1;
	}
	public void changeDirection(Direction newDir) {
		if (newDir == Direction.UP) {
			velocity.height = -1;
			velocity.width = 0;
		} else if (newDir == Direction.DOWN) {
			velocity.height = 1;
			velocity.width = 0;
		} else if (newDir == Direction.RIGHT) {
			velocity.height = 0;
			velocity.width = 1;
		} else if (newDir == Direction.LEFT) {
			velocity.height = 0;
			velocity.width = -1;
		} 
	}

	public Dimension getVelocity() {
		return velocity;
	}

	public boolean hasPower(PowerupType powerupType) {
		return powers.contains(powerupType);
	}
}
