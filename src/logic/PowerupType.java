package logic;

public enum PowerupType {
	NONE, GHOST, ZOMBIE, SPEED
}
